# Ipython

## Alpine Version
| Name    | Version       |
| ------  | ------        |
|Alpine OS| 3.13.5        |

## installed Packages
| Name             | Version    |
| ------           | ------     |
|asttokens         |2.4.1       | 
|backcall          |0.2.0       | 
|certifi           |2023.11.17  | 
|cffi              |1.16.0      | 
|charset-normalizer|3.3.2       | 
|cryptography      |41.0.7      | 
|decorator         |5.1.1       | 
|executing         |2.0.1       | 
|idna              |3.6         | 
|ipython           |8.12.3      | 
|jedi              |0.19.1      | 
|matplotlib-inline |0.1.6       | 
|parso             |0.8.3       | 
|pexpect           |4.9.0       | 
|pickleshare       |0.7.5       | 
|prompt-toolkit    |3.0.43      | 
|ptyprocess        |0.7.0       | 
|pure-eval         |0.2.2       | 
|pycparser         |2.21        | 
|Pygments          |2.17.2      | 
|requests          |2.31.0      | 
|six               |1.16.0      | 
|stack-data        |0.6.3       | 
|traitlets         |5.14.1      | 
|typing_extensions |4.9.0       | 
|urllib3           |2.1.0       | 
|wcwidth           |0.2.13      | 
