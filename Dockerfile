FROM alpine:3.13.5

WORKDIR /usr/src/app

RUN apk add --no-cache python3 libressl-dev && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi 

RUN apk --no-cache add --virtual build-deps gcc musl-dev python3-dev libffi-dev && \
    pip3 install --no-cache-dir cryptography requests ipython && \
    rm -r /root/.cache && \
    find /usr/local \
        \( -type d -a -name test -o -name tests \) \
        -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
        -exec rm -rf '{}' + && \
    apk del build-deps

RUN addgroup -g 1000 -S ipython && \
    adduser -u 1000 -S ipython -G ipython

#USER ipython

CMD ["ipython"]
